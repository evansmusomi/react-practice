import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import Todos from "./Todos";

const AppRouter = () => (
  <Switch>
    <Route path="/" exact component={Todos} />
    <Route path="/todos/:id" component={() => "Todo Detail"} />
    <Redirect to="/" />
  </Switch>
);

export default AppRouter;
