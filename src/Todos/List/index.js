import React from "react";
import TodosListItem from "./Item";

const TodosList = ({ tasks, onDelete, onComplete }) => {
  return (
    <div className="paper">
      {tasks.map(({ id, title, completed }) => (
        <TodosListItem
          key={id}
          id={id}
          title={title}
          completed={completed}
          onDelete={onDelete}
          onComplete={onComplete}
        />
      ))}
    </div>
  );
};

export default TodosList;
