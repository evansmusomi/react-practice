import React from "react";

import classes from "./ListItem.module.css";

const TodosListItem = ({ id, title, completed, onDelete, onComplete }) => {
  return (
    <div className={classes.task}>
      <div className={completed ? classes.completed : classes.incomplete}>
        {title}
      </div>
      <div className={classes.action}>
        <button
          className={classes.button}
          disabled={completed}
          onClick={() => onComplete(id)}
        >
          Complete
        </button>
        <span className={classes.cancel} onClick={() => onDelete(id)}>
          X
        </span>
      </div>
    </div>
  );
};

export default TodosListItem;
