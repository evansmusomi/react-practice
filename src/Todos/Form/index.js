import React, { useState } from "react";

const initialForm = {
  title: ""
};

const TodosForm = ({ onNewTodo }) => {
  const [todoForm, setTodoForm] = useState(initialForm);
  const [formIsValid, setFormIsValid] = useState(false);

  const handleChange = (event, name) => {
    setTodoForm({ ...todoForm, [name]: event.target.value });
    const validity = event.target.value.trim() !== "";
    setFormIsValid(validity);
  };

  const handleSubmit = event => {
    event.preventDefault();
    onNewTodo(todoForm.title);
    setTodoForm(initialForm);
    setFormIsValid(false);
  };

  return (
    <div className="paper">
      <div className="header">To-Do:</div>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <input
          type="text"
          className="textfield"
          required
          value={todoForm.title}
          onChange={event => handleChange(event, "title")}
        />
        <button className="button" type="submit" disabled={!formIsValid}>
          Add new To-do
        </button>
      </form>
    </div>
  );
};

export default TodosForm;
