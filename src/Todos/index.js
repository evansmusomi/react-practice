import React, { Component } from "react";
import TodosForm from "./Form";
import TodosList from "./List";

import api from "./services";

class Todos extends Component {
  state = {
    tasks: [],
    isLoading: false,
    error: null
  };

  handleNewTodo = task => {
    const newTodo = {
      id: Math.floor(Math.random() * 85 + 15),
      title: task,
      description: "",
      completed: false
    };

    api.post(newTodo).then(data => this.setState({ tasks: data }));
  };

  handleCompletedTodo = id => {
    api
      .update(id, { completed: true })
      .then(data => this.setState({ tasks: data }));
  };

  handleDeletedTodo = id => {
    api.delete(id).then(data => this.setState({ tasks: data }));
  };

  componentDidMount() {
    this.setState({ isLoading: true });
    api
      .get()
      .then(data => this.setState({ tasks: data, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));
  }

  renderTodos = () => {
    const { tasks, isLoading, error } = this.state;
    let content;

    if (error) {
      content = <p>{error.message}</p>;
    } else if (isLoading) {
      content = <p>Loading...</p>;
    } else {
      content = (
        <TodosList
          tasks={tasks}
          onDelete={this.handleDeletedTodo}
          onComplete={this.handleCompletedTodo}
        />
      );
    }

    return content;
  };

  render() {
    return (
      <div className="screen">
        <TodosForm onNewTodo={this.handleNewTodo} />
        {this.renderTodos()}
      </div>
    );
  }
}

export default Todos;
