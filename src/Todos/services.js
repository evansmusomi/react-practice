const BASE_URL = "https://practiceapi.devmountain.com/api/";

const api = {
  get: () =>
    fetch(`${BASE_URL}/tasks`).then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("Something went wrong...");
      }
    }),
  delete: item =>
    fetch(`${BASE_URL}/tasks/${item}`, {
      method: "DELETE"
    }).then(response => response.json()),
  update: (item, data) =>
    fetch(`${BASE_URL}/tasks/${item}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(response => response.json()),
  post: data =>
    fetch(`${BASE_URL}/tasks`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(response => response.json())
};

export default api;
