# React To-Do App

A minimalist To-Do app solution to the DevMountain React challenge.

- Add a new todo
- See all todos
- Complete a todo
- Delete a todo

Switch to the Redux branch for a Redux version of this app.
